# hybris-training

This is an ongoing Hybris training course. The lessons start with the initial setup of a basic Hybris site and they will increase in difficulty and complexity as different concepts (related to a typical ecommerce operation) are introduced.

## Getting Started

These instructions will help you get the project up and running on your local machine for development and testing purposes. Please follow the instructions under [Local Environment Setup page](https://talosdigital.atlassian.net/wiki/spaces/HYBRIS/pages/191528990) to properly configure all the required tools.

### Prerequisites

Before installing the project, make sure you have the following:

* Hybris Commerce latest version.
* Java 1.8+
* MySQL latest version.
* MySQL driver for Hybris.
* ImageMagick latest version.
* A local database and a user with permissions on your local schema.

### Host File
* 127.0.0.1 urbancorner.local

### Installing

* Open a terminal, and create a workspace folder i.e. `<workspace-name>` at your preferred path
* cd `<workspace-name>`
* unzip `<path to hybris zip file>` 
* Remove **config** folder under `hybris/bin` path.
* Remove  **custom** folder under `/hybris` path.
* git clone https://github.com/talosdigital/hybris-training.git
* Add an env variable pointing to your custom config folder `export HYBRIS_OPT_CONFIG_DIR=/path-to-repo/hybris/config-local`. Depending on the environment you could use **config-local**, **config-dev**,**config-qa**, **config-staging** or **config-prod**. 
* execute the **setantenv.sh** or **setantenv.bat** from your `/bin/platform` folder
* cd `<workspace-name>`/hybris-training
* At this location you should have access to a custom **build.xml** file with the following content:

```xml
<?xml version="1.0" encoding="utf-8"?>
<project name="uc">
    <description>
           Prepare the hybris environment - Based on the existing config resources from respository.
           First uses 'common' folder and then merges environment specific files into it.
    </description>
    <property environment="env"/>
    <property name="platformhome" location="${env.PLATFORM_HOME}"/>
    <property name="HYBRIS_HOME" location="hybris"/>
    <property name="HYBRIS_CONFIG_DIR" location="${HYBRIS_HOME}/config"/>
    <property name="environment" value="local"/>
    <property name="HYBRIS_ENV_CONFIG_DIR" location="${HYBRIS_HOME}/config-${environment}"/>
    <property name="HYBRIS_COMMON_CONFIG_DIR" location="${HYBRIS_HOME}/config-common"/>
    <property name="template" value="develop"/>
    <available file="${platformhome}/build.xml" property="platformPresent"/>
    <echo>-------------------------------------------------------------------------------</echo>
    <echo>ENVIRONMENT: ${environment}</echo>
    <echo>platformhome: ${platformhome}</echo>
    <echo>-------------------------------------------------------------------------------</echo>
    <target name="envconfig">
        <available file="${platformhome}/build.xml" property="platformPresent"/>
        <antcall target="prepareconfig"/>
    </target>
    <target name="prepareconfig" if="platformPresent">
        <ant dir="${platformhome}/resources/ant" target="prepare">
            <property name="input.template" value="${template}"/>
        </ant>
        <echo>Overwriting local config resources using ${HYBRIS_COMMON_CONFIG_DIR}</echo>
        <copy todir="${HYBRIS_CONFIG_DIR}" overwrite="true">
            <fileset dir="${HYBRIS_COMMON_CONFIG_DIR}"/>
        </copy>
    </target>
</project>
```

* run `ant envconfig`. 
* The previous step will create a config folder which content depends on the template variable and will use the content defined at **/config-common** folder. Notice that content from config-{env} will override properties at runtime
* Create a soft link for **config** and **custom** folders

```
ln -s path-to-repo/hybris/config path-to-hybris-unzipped/hybris/config
```
```
ln -s path-to-repo/hybris/bin/custom path-to-hybris-unzipped/hybris/bin/custom
```

* Change the values at your **config-local/10-local.properties** file
* Make sure you don't commit your local changes to the origin branch for the file located at **config-local/10-local.properties**
* cd `<hybris-unzipped-folder>`/hybris/bin/platform
* execute the **setantenv.sh** or **setantenv.bat**
* run `ant clean all`
* Don't forget to add the proper entries to your **hosts** file
* run `ant initialize`
* Start hybris by running `./hybrisserver.sh` or `./hybrisserver.bat`


For more information visit [Environment Configuration Management wiki page](https://wiki.hybris.com/display/hybrisALF/Environment+Configuration+Management)
