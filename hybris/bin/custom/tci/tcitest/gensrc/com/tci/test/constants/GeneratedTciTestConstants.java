/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Jul 31, 2018 10:04:05 AM                    ---
 * ----------------------------------------------------------------
 */
package com.tci.test.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedTciTestConstants
{
	public static final String EXTENSIONNAME = "tcitest";
	
	protected GeneratedTciTestConstants()
	{
		// private constructor
	}
	
	
}
