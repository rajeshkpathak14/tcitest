/*
 * [y] hybris Platform
 *
 * Copyright (c) 2018 SAP SE or an SAP affiliate company.  All rights reserved.
 *
 * This software is the confidential and proprietary information of SAP
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with SAP.
 */
package com.tci.facades.constants;

/**
 * Global class for all TciFacades constants.
 */
public class TciFacadesConstants extends GeneratedTciFacadesConstants
{
	public static final String EXTENSIONNAME = "tcifacades";

	private TciFacadesConstants()
	{
		//empty
	}
}
